const {Router} = require('express');


module.exports =  function({LabController}){
    const router =  Router();
    router.get('/:labId', LabController.get);
    router.get('', LabController.getAll );
    router.post('/', LabController.create);
    router.patch('/:labId', LabController.update );
    router.delete('/:labId', LabController.delete);
    return router;

}
