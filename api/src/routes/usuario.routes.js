const {Router} = require('express');


module.exports =  function({UsuarioController}){
    const router =  Router();
    router.get('/:usuarioId', UsuarioController.get  );
    router.get('', UsuarioController.getAll );
    router.post('/', UsuarioController.create);
    router.patch('/:usuarioId', UsuarioController.update );
    router.delete('/:usuarioId', UsuarioController.delete);
    return router;

}
