const {Router} = require('express');


module.exports =  function({EquiController}){
    const router =  Router();
    router.get('/:equiId', EquiController.get);
    router.get('', EquiController.getAll );
    router.post('/', EquiController.create);
    router.patch('/:equiId', EquiController.update );
    router.delete('/:equiId', EquiController.delete);
    return router;

}
