const {Router} = require('express');


module.exports =  function({AreaController}){
    const router =  Router();
    router.get('/:areaId', AreaController.get);
    router.get('', AreaController.getAll );
    router.post('/', AreaController.create);
    router.patch('/:areaId', AreaController.update );
    router.delete('/:areaId', AreaController.delete);
    return router;

}
