module.exports = {
    HomeRoutes: require("./home.routes"),
    LabRoutes: require("./lab.routes"),
    EquiRoutes: require("./equi.routes"),
    AreaRoutes: require("./area.routes"),
    UsuarioRoutes: require("./usuario.routes")
}
