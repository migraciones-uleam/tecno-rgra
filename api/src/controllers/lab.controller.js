let _labService = null;
class labController
{
    constructor({LabService})
    {
        _labService = LabService;
    }
    //TODO: mEtodos CRUD
    //get
    async get(req,res)
    {
        const {labId} = req.params;
        const lab=  await  _labService.get(labId);
        return  res.send(lab);
    }
    //getAll
    async getAll(req,res)
    {
        const labs =  await  _labService.getAll();
        return res.send(labs);
    }
    //create
    async create(req, res)
    {
        const {body} = req;
        const createdLab =  await  _labService.create(body)
        res.send(createdLab);
    }
    //update
    async update(req,res)
    {
        const {body} =  req;
        const { labId } =  req.params;
        const updatedLab =  await _labService.update( labId, body );
        res.send(updatedLab);
    }
    //delete
    async delete(req,res)
    {
        const { labId } = req.params;
        const deletedLab =  await _labService.delete(labId);
        return res.send(deletedLab);
    }

}
module.exports= labController;