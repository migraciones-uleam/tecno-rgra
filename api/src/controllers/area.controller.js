let _areaService = null;
class AreaController
{
    constructor({AreaService})
    {
        _areaService = AreaService;
    }
    //TODO: mEtodos CRUD
    //get
    async get(req,res)
    {
        const {areaId} = req.params;
        const area=  await  _areaService.get(areaId);
        return  res.send(area);
    }
    //getAll
    async getAll(req,res)
    {
        const areas =  await  _areaService.getAll();
        return res.send(areas);
    }
    //create
    async create(req, res)
    {
        const {body} = req;
        const createdArea =  await  _areaService.create(body)
        res.send(createdArea);
    }
    //update
    async update(req,res)
    {
        const {body} =  req;
        const { areaId } =  req.params;
        const updatedArea =  await _areaService.update( areaId, body );
        res.send(updatedArea);
    }
    //delete
    async delete(req,res)
    {
        const { areaId } = req.params;
        const deletedArea =  await _areaService.delete(areaId);
        return res.send(deletedArea);
    }

}
module.exports= AreaController;