const mongoose  =  require('mongoose') 
const { Schema } = mongoose;


const LabSchema =   new Schema({
    name : { type:String, required: true },
    capacidad : {type:String , required:true},
    observacion : {type:String , required:true}
})
LabSchema.methods.toJSON = function(){
    let lab = this.toObject();
    return lab;
}
module.exports = mongoose.model('lab', LabSchema);