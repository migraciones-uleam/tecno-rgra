const mongoose  =  require('mongoose') 
const { Schema } = mongoose;


const EquiSchema =   new Schema({
   cantidad : { type:String, required: true },
   nombre: { type:String, required: true },
     descripcion: {type:String , required:true},
})
EquiSchema.methods.toJSON = function(){
    let equi = this.toObject();
    return equi;
}
module.exports = mongoose.model('equi', EquiSchema);