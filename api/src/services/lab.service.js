const BaseService =  require('./base.service')

let _labRepository = null;

class LabService extends BaseService
{
    constructor({LabRepository})
    {
        super(LabRepository);
        _labRepository = LabRepository;
    }
    async  getLabByname(name)
     {
         return await  _labRepository.getLabByname(name);
    }

}
module.exports = LabService;
