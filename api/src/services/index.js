module.exports = {
    HomeService: require('./home.service'),
    LabService: require('./lab.service'),
    UsuarioService: require('./usuario.service'),
    EquiService: require('./equi.service'),
    AreaService: require('./area.service')
}
