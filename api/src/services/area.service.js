const BaseService =  require('./base.service')

let _areaRepository = null;

class AreaService extends BaseService
{
    constructor({AreaRepository})
    {
        super(AreaRepository);
        _areaRepository = AreaRepository;
    }
    async  getAreaByname(name)
     {
         return await  _areaRepository.getAreaByname(name);
    }

    
}
module.exports = AreaService;