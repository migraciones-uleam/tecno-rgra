const BaseRepository  = require('./base.repository')

let _equi= null;

class EquiRepository extends BaseRepository
{
    constructor({Equi})
    {
        super(Equi);
        _equi = Equi;

    }
    async getEquiByname(name)
    {
        return await _equi.findOne({name});
    }

}
module.exports = EquiRepository;