var express = require('express');
var router = express.Router();

const { EquiController } = require('../controllers/index');

router.get('/', EquiController.getEqui)
router.get('/nuevoE', EquiController.updateNuevoE);
router.post('/', EquiController.postEqui);
router.get('/:id', EquiController.updateEquipoForm);
router.put('/:id', EquiController.updateEqui);
router.delete('/:id', EquiController.deleteEqui);

module.exports = router;
