var express = require('express');
var router = express.Router();

const { LabController } = require('../controllers/index');

router.get('/', LabController.getLab);
router.get('/nuevoL', LabController.updateNuevoL);
router.post('/', LabController.postLab);
router.get('/:id', LabController.updateLaboratorioForm);
router.put('/:id', LabController.updateLab);
router.delete('/:id', LabController.deleteLab);

module.exports = router;
