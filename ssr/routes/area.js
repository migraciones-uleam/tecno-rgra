var express = require('express');
var router = express.Router();

const { AreaController } = require('../controllers/index');

router.get('/', AreaController.getArea);
router.get('/nuevoA', AreaController.updateNuevoA);
router.post('/', AreaController.postArea);
router.get('/:id', AreaController.updateAreaForm);
router.put('/:id', AreaController.updateArea);
router.delete('/:id', AreaController.deleteArea);

module.exports = router;
