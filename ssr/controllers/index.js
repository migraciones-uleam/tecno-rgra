module.exports = {
    AreaController: require('./area.controller'),
    LabController: require('./lab.controller'),
    UsuarioController: require('./user.controller'),
    EquiController: require('./equi.controller')
}